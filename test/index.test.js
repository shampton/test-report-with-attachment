const expect = require('chai').expect;
const sum = require('../index');

describe('index.js', function () {
  it('adds 1 + 2 to equal 3', function () {
    expect(sum(1, 2)).to.equal(3);
  });

  it('should fail this test on purpose', function () {
    this.test.attachments = ['cat_synth.jpg'];
    expect(sum(1, 2)).to.equal(4);
  });
});
